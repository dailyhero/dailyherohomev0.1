/*global angular*/
'use strict';

/**
*Mockup Interceptor es un interceptador http que permite simular respuestas a servicios REST. El camino usual es a través del request,
se hace fallar la consulta por timeout y de esta forma vuelve a Angular como error, no obstante, desde aquí la consulta se puede dar la 
configuración que se quiera, por ejemplo se puede aparentar que la respuesta fue correcta con estado 200, o bien que falló pero por otra razón (500,
400, 404, etc). El comportamiento actual se realiza de forma aleatoria con un porcentaje que no debe superar la variable 'successResponseRatio', si
lo supera, se simula una respuesta de error. 

@param {int} successResponseRatio Variable que determina el porcentaje de error de una consulta simulada o mockeada.
*/

angular.module('heroHome.mockups')
.constant('mockupService', {})
.factory('mockupInterceptor', ['$q', 'mockupService', function($q, mockupService) {
  var successResponseRatio= 100;

  var interceptor = {
    request: function(config) {
      if(window.cordova && !config.mockAnyway){
        return config;
      }
      if (config.mockup && mockupService[config.mockup]) {
        config.timeout = 1500;
      }

      return config;
    },
    responseError : function(rejection){
      if( (window.cordova || !rejection.config.mockup) && !rejection.config.mockAnyway ){
        return $q.reject(rejection);
      }

      var typeMockup= rejection.config.mockup; 
      var random= Math.floor(Math.random() * 100) +1 ;

      if(random <= successResponseRatio){
        rejection.status= 200;
        random = Math.floor(Math.random() * mockupService[typeMockup].responseSuccess.length);
        rejection.data = mockupService[typeMockup].responseSuccess[random].response;
        return $q.resolve(rejection);
      }
      else{
        if(mockupService[typeMockup].responseError){
          rejection.status= 400;
          random = Math.floor(Math.random() * mockupService[typeMockup].responseError.length);
          rejection.data = mockupService[typeMockup].responseError[random].response; 
        } else{
          rejection.status = 500;
        } 
        return $q.reject(rejection);
      }
    }
  };

  return interceptor;

}])

.config(['$httpProvider', function($httpProvider) {
  $httpProvider.interceptors.push('mockupInterceptor');
}]);