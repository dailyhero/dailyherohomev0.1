/*global angular*/
'use strict';

angular.module('heroHome.mockups')
	.config(['mockupService',
		function(mockupService){
			mockupService.logIn = {
				responseSuccess : [
					{
						response:{
							token: '12r5qwgbsdtwe434y24w3h34yh54jjrt54jr54jt'
						},
						output: {

						}
					}
				]
			}
		}
	])