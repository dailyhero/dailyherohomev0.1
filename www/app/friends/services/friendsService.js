angular.module('heroHome.services')

// Friends service
.factory('Friends', function () {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [
    {
      id: 0,
      name: 'Ben Sparrow',
      face: 'img/thumb/ben.png'
    },
    {
      id: 1,
      name: 'Max Lynx',
      face: 'img/thumb/max.png'
    },
    {
      id: 2,
      name: 'Adam Bradleyson',
      face: 'img/thumb/adam.jpg'
    },
    {

      d: 3,
      name: 'Perry Governor',
      face: 'img/thumb/perry.png'
    },
    {
      id: 4,
      name: 'Mike Harrington',
      face: 'img/thumb/mike.png'
    },
    {
      id: 5,
      name: 'Ben Sparrow',
      face: 'img/thumb/ben.png'
    },
    {
      id: 6,
      name: 'Max Lynx',
      face: 'img/thumb/max.png'
    }
  ];

  return {
    all: function () {
      return friends;
    },
    remove: function (post) {
      friends.splice(friends.indexOf(post), 1);
    }
  };
})
