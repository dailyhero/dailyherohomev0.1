/*global angular*/
'use strict';

angular.module('heroHome.services')


/* Servicio que permite administrar la autentifícación
*
*/
.factory('authService', ['$http', '$q', '$window', '$state', 'Config', function($http, $q, $window, $state, Config ){
    var auth = {};
  var tokenName = 'dailyheroHomeToken';

  /*Guarda el token de forma persistente en storage
  * @param {string} token Cadena de texto 
    @param {boolean} rememberSession Boleano que indica si se desea preservar la sesion en localStorage, si es falso, solo se guarda en sessionStorage.
  */
  auth.saveToken = function (token, rememberSession){
    if (rememberSession) {
      if ($window.sessionStorage[tokenName]) { $window.sessionStorage.removeItem(tokenName); }
      $window.localStorage[tokenName] = token;
    } else {
      if ($window.localStorage[tokenName]) { $window.localStorage.removeItem(tokenName); }
      $window.sessionStorage[tokenName] = token;
    }
    
  };

/*Permite obtener el token que está guardado en localStorage o sessionStorage.
*@return {string} Devuelve el token 
*/
  auth.getToken = function (){
    return $window.localStorage[tokenName] ? $window.localStorage[tokenName] : $window.sessionStorage[tokenName];
  }

/*Permite determinar si un usuario tiene un token asociado.
*@return {boolean} Corresponde a la respuesta 
*/
  auth.isLoggedIn = function(){
    var token = auth.getToken();

    if(token){
      var payload = JSON.parse($window.atob(token.split('.')[1]));
      return payload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  };
  
/*Determina si un usuario esta logeado, sino lo está redirige hacia el home.
*
*/ 
  auth.checkLoggedIn = function(){
    if (!auth.isLoggedIn()) { $state.go('home', {}, {reload: true}); }
  };
  

/*Permite obtener información de usuario que se encuentra contenida en el token
*@return {object} payload Corresponde a un objeto que contiene el 'id' y el 'username'
*/  
  auth.currentUser = function(){
    if(auth.isLoggedIn()){
      var token = auth.getToken();
      var payload = JSON.parse($window.atob(token.split('.')[1]));
      return payload; //objeto con 'id' y el 'username'
    }
  };
  
/*Funcion que permite logearse a un usuario
*@param {string} user Corresponde al nombre de usuario o email
@param {string} password Corresponde a la contraseña del usuario (encriptada)
@param {boolean} rememberSession Corresponde a un boleano que indica si se debe guardar en localStorage(true) o en sessionStorage(false) 
@return {promise} Si la promesa se cumple satisfactoriamente devuelve un boleano, sino devuelve, un objeto con un estado y mensaje de error.
*/
  auth.logIn = function(user, password, rememberSession){
    var defer= $q.defer();
    var requestData = {
      user: user,
      password: password
    };

    $http({
        method: 'POST',
        url: '/login',
        mockup: 'logIn',
        mockAnyway: true,
        data: requestData,
        timeout: Config.timeout 
      }).then(function(response) {
        var token= response.data.token;
        auth.saveToken(token, rememberSession);
        defer.resolve(true);
      }).catch(function(error){
        defer.reject(error);
      });

    return defer.promise;
  };
  
  /*Permite desconectar a un usuario de su sesión. Se borra su token del Storage.
  * 
  */
  auth.logOut = function(){
    if ($window.localStorage[tokenName]) { $window.localStorage.removeItem(tokenName); }
    else if ($window.sessionStorage[tokenName]) { $window.sessionStorage.removeItem(tokenName); }
    $state.go('home', {}, {reload: true});
  };

  return auth;
}])


