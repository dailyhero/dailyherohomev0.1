/*global angular*/
'use strict';

angular.module('heroHome.controllers')
.controller('AuthCtrl', ['$scope', '$state', '$ionicPopup', 'authService', 'Config', function($scope, $state, $ionicPopup, authService, Config) {
  
  // Ocultar el boton 'back'
  /*$ionicHistory.nextViewOptions({
    disableBack: true
  });*/
  
  /*Levanta una ventana popup con un mensaje de error
  *@param {object} error Se espera un objeto de error {status: '', msg_error : ''} 
  */
  var showMsgError= function(error){
  	var msg_error= Config.errors.default;
    if(error.status === 400){
      msg_error= error.msg_error;
    }
  	var alertPopup= $ionicPopup.alert({
  		template: msg_error
  	});
  };

  /*
  *Login de usuario
  @param {string} email
  @param {string} password 
  */
  $scope.login= function(email, password){

  	var rememberSession= false; 
  	authService.logIn(email, password, rememberSession)
  		.then(function(token){
  			$state.go('home');
  		})
  		.catch(function(error){
  			showMsgError(error);
  		});


  };

}]);
