angular.module('heroHome.services')

// Photos service
.factory('Photos', function () {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var photos = [
    {
      src: "img/gallery/p1.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p2.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p3.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p4.png",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p5.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p6.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p7.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p8.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p9.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
  ];

  return {
    all: function () {
      return photos;
    },
    remove: function (post) {
      photos.splice(photos.indexOf(post), 1);
    }
  };
})