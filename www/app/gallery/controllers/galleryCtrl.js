angular.module('heroHome.controllers')

// Gallery controller
.controller('GalleryCtrl', function($scope, $state, Photos) {
  // get list photos from Photos service
  $scope.photos = Photos.all();

})
