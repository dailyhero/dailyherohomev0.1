angular.module('heroHome.services')

.factory('Places', function () {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var places = [
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r1_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
    {
      name: "Hanoi hotel",
      thumb: "img/thumb/r2_thumb.jpg",
      type: "Hotel",
      address: "No 2, Hoang Sa",
      distance: "120m",
      rate: 9.1
    },
    {
      name: "Chipa Chipa BBQ",
      thumb: "img/thumb/r3_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Truong Sa",
      distance: "220m",
      rate: 7.5
    },
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r4_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r5_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r6_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r7_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
  ];

  return {
    all: function () {
      return places;
    },
    remove: function (post) {
      places.splice(places.indexOf(post), 1);
    },
    get: function (postId) {
      for (var i = 0; i < places.length; i++) {
        if (places[i].id === parseInt(postId)) {
          return places[i];
        }
      }
      return null;
    }
  };
})
