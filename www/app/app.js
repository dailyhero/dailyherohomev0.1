/*global angular*/
'use strict';

angular.module('heroHome', ['ionic',
  'heroHome.constants', 
  'heroHome.mockups',
  'heroHome.controllers', 
  'heroHome.services',
  'nl2br', // 'nl2brl' is found in lib/angular-nl2br
  'monospaced.elastic', // 'monospaced.elastic' is found in lib/angular-elastic
  'ion-gallery']) // 'ion-gallery' is found in lib/ion-gallery

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {


  $stateProvider
   // login screen
  .state('login', {
    url: '/login',
    templateUrl: 'app/auth/templates/login.html',
    controller: 'AuthCtrl'
  })

  // register screen
  .state('register', {
    url: '/register',
    templateUrl: 'app/auth/templates/register.html',
    controller: 'AuthCtrl'
  })

  // Home screen
  .state('home', {
    url: '/home',
    templateUrl: 'app/home/templates/home.html',
    controller: 'HomeCtrl'
  })

  // City information
  .state('information', {
    url: '/information',
    templateUrl: 'app/information/templates/information.html',
    controller: 'InformationCtrl'
  })

  // Show nearby places
  .state('nearby', {
    url: '/nearby',
    templateUrl: 'app/nearby/templates/nearby.html',
    controller: 'NearbyCtrl'
  })

  // Book flight ticket
  .state('ticket', {
    url: '/ticket',
    templateUrl: 'app/ticket/templates/ticket.html',
    controller: 'TicketCtrl'
  })

   // Weather
  .state('weather', {
    url: '/weather',
    templateUrl: 'app/weather/templates/weather.html',
    controller: 'WeatherCtrl'
  })

   // Gallery
  .state('gallery', {
    url: '/gallery',
    templateUrl: 'app/gallery/templates/gallery.html',
    controller: 'GalleryCtrl'
  })

  // Friends nearby
  .state('friends', {
    url: '/friends',
    templateUrl: 'app/friends/templates/friends.html',
    controller: 'FriendsCtrl'
  })

  // Book a taxi
  .state('taxi', {
    url: '/taxi',
    templateUrl: 'app/taxi/templates/taxi.html',
    controller: 'TaxiCtrl'
  })

  // Chat list
  .state('chats', {
    url: '/chats',
    templateUrl: 'app/chats/templates/chats.html',
    controller: 'ChatCtrl'
  })

  .state('chat-detail', {
    url: '/chats/:chatId',
    templateUrl: 'app/chats/templates/chat-detail.html',
    controller: 'ChatDetailCtrl'
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

});
