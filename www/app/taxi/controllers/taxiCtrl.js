angular.module('heroHome.controllers')

.controller('TaxiCtrl', function($scope, $state) {
    function initialize() {
      // set up begining position
      var myLatlng = new google.maps.LatLng(21.0227358,105.8194541);

      // set option for map
      var mapOptions = {
        center: myLatlng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      // init map
      var map = new google.maps.Map(document.getElementById("map"),
        mapOptions);

      // assign to stop
      $scope.map = map;
    }
    // load map when the ui is loaded
    $scope.init = function() {
      initialize();
    }
})