angular.module('heroHome.controllers')

// Weather controller
.controller('WeatherCtrl', function($scope, $state, Weather) {
   // get list days from Weather model
  $scope.days = Weather.all();

  // today weather
  $scope.today = $scope.days[0];
})