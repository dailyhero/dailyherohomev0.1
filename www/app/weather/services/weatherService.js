angular.module('heroHome.services')

.factory('Weather', function () {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var days = [
    {
      weekDay: "Monday",
      minTemp: 10,
      maxTemp: 20,
      currentTemp: 19,
      feelLike: 19,
      status: "Cloudy",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
    {
      weekDay: "Tuesday",
      minTemp: 11,
      maxTemp: 20,
      currentTemp: 18,
      feelLike: 18,
      status: "Cloud",
      wind: {
        speed: 21,
        direction: "up-left"
      }
    },
    {
      weekDay: "Wednesday",
      minTemp: 15,
      maxTemp: 25,
      currentTemp: 22,
      feelLike: 22,
      status: "Fog",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
    {
      weekDay: "Thursday",
      minTemp: 10,
      maxTemp: 20,
      currentTemp: 19,
      feelLike: 19,
      status: "Day-sunny",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
    {
      weekDay: "Friday",
      minTemp: 10,
      maxTemp: 20,
      currentTemp: 19,
      feelLike: 19,
      status: "Day-lightning",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
    {
      weekDay: "Saturday",
      minTemp: 10,
      maxTemp: 20,
      currentTemp: 19,
      feelLike: 19,
      status: "Cloudy",
      wind: {
        speed: 21,
        direction: "down-right"
      }
    },
    {
      weekDay: "Sunday",
      minTemp: 5,
      maxTemp: 15,
      currentTemp: 15,
      feelLike: 12,
      status: "Rain",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
  ];

  return {
    all: function () {
      return days;
    },
    remove: function (post) {
      days.splice(days.indexOf(post), 1);
    },
    get: function (postId) {
      for (var i = 0; i < days.length; i++) {
        if (days[i].id === parseInt(postId)) {
          return days[i];
        }
      }
      return null;
    }
  };
})
