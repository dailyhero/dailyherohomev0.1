/*global angular*/
'use strict';

angular.module('heroHome.constants')
.constant('Config', {
		SERVER_URL: 'localhost/',
		errors: {
			default: 'Se han presentado problemas, por favor, intente más tarde'
		},
		timeout: 15000
	}
);

